import sqlite3 as lite


def execute_query(query, db='dbdb'):
    db = lite.connect(db)
    db.execute(query)
    db.commit()
    db.close()


def create_table(table_name, columns):

    cols_declaration = ',\n'.join(
        (f'{col_name} {col_type}'
         for col_name, col_type in columns.items())
    )
    if cols_declaration:
        query = f"""
            CREATE TABLE IF NOT EXISTS {table_name} (
                id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
                {cols_declaration}
            );
        """
        execute_query(query)


def create_record(table_name, columns, values):
    values = [str(value) for value in values]
    query = f"""
        INSERT INTO {table_name} ({','.join(columns)})
        VALUES ({','.join(values)});
    """
    print(query)
    execute_query(query)