from pyMeta import ORMMeta
from pyTyped import Typed


class Model(metaclass=ORMMeta):
    pass


class IntegerField(Typed):
    type = int

