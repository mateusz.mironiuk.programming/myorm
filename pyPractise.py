import pyModel
import pyDB


class TableName(pyModel.Model):
    integer1 = pyModel.IntegerField('integer1')
    integer2 = pyModel.IntegerField('integer2')

    def __init__(self, integer1, integer2):
        self.integer1 = integer1
        self.integer2 = integer2


o = TableName(6000, 3000)
print(o.integer2, o.integer1)

pyDB.execute_query(query='SELECT * FROM TableName')