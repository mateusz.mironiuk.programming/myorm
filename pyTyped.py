sqltypes = {int: 'INTEGER'}


class Typed:
    type = None

    def __init__(self, name):
        self.name = name

    def __get__(self, instance, owner):
        return instance.__dict__[self.name]

    def __set__(self, instance, value):
        if not isinstance(value, self.type):
            raise TypeError(f'Expected {self.type.__name__}')
        instance.__dict__[self.name] = value

    @property
    def sqltype(self):
        return f'{sqltypes[self.type]}'
