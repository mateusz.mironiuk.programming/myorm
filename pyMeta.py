from pyTyped import Typed
from pyDB import create_table, create_record


class ORMMeta(type):
    def __new__(mcs, clsname, bases, clsdict):
        print(f'clsdict {clsdict}')
        table_name = clsname
        fields = {c: f.sqltype for c, f in clsdict.items()
                  if isinstance(f, Typed)}
        attrs_names = tuple(fields.keys())
        if fields:
            create_table(table_name, fields)
        clsobj = super().__new__(mcs, clsname, bases, clsdict)

        def __new__(cls, *args, **kwargs):
            nonlocal table_name
            nonlocal attrs_names
            attrs_values = mcs._get_attr_vals(attrs_names, args, kwargs)
            instance = object.__new__(cls)  # TODO: Make it super()
            create_record(table_name=table_name,
                          columns=attrs_names,
                          values=attrs_values)
            return instance

        clsobj.__new__ = __new__
        return clsobj

    @staticmethod
    def _get_attr_vals(attrs_names: tuple, args: tuple, kwargs: dict):
        return tuple(
            list(args) + [kwargs[name] for name in attrs_names[len(args):]]
        )  # TODO Check if extend is not faster
